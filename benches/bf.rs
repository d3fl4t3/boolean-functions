use bencher::{benchmark_group, benchmark_main, Bencher};

use bf::BF;

fn bf_weight(bench: &mut Bencher) {
    let bf = BF::random(31);
    bench.bench_n(1, |_| ());
    bench.iter(|| {
        bf.weight();
    });
}

fn bf_mobius(bench: &mut Bencher) {
    let mut f = BF::random(31);
    bench.bench_n(1, |_| ());
    bench.iter(|| {
        f.transform_mobius();
    });
}

benchmark_group!(benches, bf_weight);
benchmark_main!(benches);
