use super::bit::Bit;
use super::utils::*;
use std::convert::TryInto;
use std::error::Error;
use std::fmt::{Display, Formatter};

/// Ошибка булевой функции
#[derive(Debug, Clone)]
pub struct BFError;
impl Error for BFError {}
impl Display for BFError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("BFError")?;
        Ok(())
    }
}

type BFResult<T> = Result<T, Box<dyn Error>>;

/// Класс булевой функции
#[derive(PartialEq, Clone)]
pub struct BF {
    values: Vec<u32>,
    arg_count: u32,
}

impl BF {
    /// Создание булевой функции из вектора значений.
    ///
    /// Возвращает ошибку если:
    /// 1) `values.len() != (1 << n)`
    /// 2) `values` содержит что-то кроме `'0'` и `'1'`
    /// 3) `values.len()` больше чем может поместиться в `u32`
    pub fn from_string(str_values: &str) -> BFResult<Self> {
        if !test_exp2(str_values.len().try_into()?) {
            return Err(Box::new(BFError));
        }
        if str_values.chars().any(|c| !"01".contains(c)) {
            return Err(Box::new(BFError));
        }
        let arg_count = log2(str_values.len() as u32);
        Ok(Self::from_function(
            |x| match str_values.chars().nth(x as usize).unwrap() {
                '0' => Bit::Zero,
                '1' => Bit::One,
                _ => unreachable!(),
            },
            arg_count,
        ))
    }

    /// Создание булевой функции, соответствующей функции gen_function
    pub fn from_function<F>(gen_function: F, arg_count: u32) -> Self
    where
        F: Fn(u32) -> Bit,
    {
        let mut values: Vec<u32> = Vec::new();
        let mut tmp = 0;
        for i in 0..exp2(arg_count) {
            let pos = mod_word_bit_size(i as u32);
            match gen_function(i) {
                Bit::One => set_bit(&mut tmp, pos),
                Bit::Zero => unset_bit(&mut tmp, pos),
            }
            if mod_word_bit_size(i as u32 + 1) == 0 || i == exp2(arg_count) - 1 {
                values.push(tmp);
                tmp = 0;
            }
        }
        Self { values, arg_count }
    }

    /// Измерение веса булевой функции
    pub fn weight(&self) -> u32 {
        let mut weight = 0;
        for &v in &self.values {
            let mut v = v;
            while v > 0 {
                weight += 1;
                v = without_last_bit(v);
            }
        }
        weight
    }

    /// Поиск значения булевой функции по битовому вектору аргументов
    ///
    /// Возвращает ошибку если длина битового вектора больше
    /// количества аргументов булевой функции
    pub fn evaluate(&self, input: u32) -> BFResult<Bit> {
        if input >= exp2(self.arg_count) {
            return Err(Box::new(BFError));
        }
        Ok(Bit::from_u32(
            self.values[div_word_bit_size(input) as usize] >> mod_word_bit_size(input) & 1,
        ))
    }

    /// Строковое представление вектора значений
    pub fn to_string(&self) -> String {
        (0..exp2(self.arg_count))
            .map(|x| self.evaluate(x))
            .map(BFResult::unwrap)
            .map(|x| match x {
                Bit::Zero => '0',
                Bit::One => '1',
            })
            .collect()
    }

    /// Константа 0
    pub fn const0(arg_count: u32) -> Self {
        let word_count = div_word_bit_size(exp2(arg_count))
            + if mod_word_bit_size(exp2(arg_count)) > 0 {
                1
            } else {
                0
            };
        let values = vec![0u32; word_count as usize];
        Self { arg_count, values }
    }

    /// Константа 1
    pub fn const1(arg_count: u32) -> Self {
        let last_word_len = mod_word_bit_size(exp2(arg_count));
        let mut values = vec![u32::MAX; div_word_bit_size(exp2(arg_count)) as usize];
        if last_word_len > 0 {
            values.push(!exp2(last_word_len));
        }
        Self { arg_count, values }
    }

    /// Случайная функция
    pub fn random(arg_count: u32) -> Self {
        let mut values: Vec<u32> = Vec::new();
        let last_word_len = mod_word_bit_size(exp2(arg_count));
        for _ in 0..div_word_bit_size(exp2(arg_count)) {
            values.push(rand::random());
        }
        if last_word_len > 0 {
            values.push(rand::random::<u32>() & (exp2(last_word_len) - 1));
        }
        Self { arg_count, values }
    }

    /// Применить к функции преобразование Мёбиуса
    pub fn transform_mobius(&mut self) {
        // Алгоритм из методички, применяем для блоков по 32 бита
        const MASKS: [u32; 5] = [
            0b10101010101010101010101010101010,
            0b11001100110011001100110011001100,
            0b11110000111100001111000011110000,
            0b11111111000000001111111100000000,
            0b11111111111111110000000000000000,
        ];
        for v in &mut self.values {
            for i in 0..5 {
                *v ^= (*v << exp2(i)) & MASKS[i as usize];
            }
        }

        // Дальше алгоритм будет примерно такой:
        //
        // v[i+1..i] ^= ((v[i+1..i]) << 32) & (старшие 32 бита)  (где i делится на 2)
        // эквивалентно: v[i+1] ^= v[i]
        //
        // v[i+3..i] ^= (v[i+3..i] << 64) & (старшие 64 бита) (где i делится на 4)
        // эквивалентно: v[i+3..i+2] ^= v[i+1..i]
        //
        // Далее i должно делиться на 8, а формула будет такой:
        // v[i+7..i+4] ^= v[i+3..i]
        //
        // Предпоследний раз i будет равно 0 и self.values.len() / 2
        // Последний раз i будет равно только 0

        for group_size in 1..log2(self.values.len() as u32) + 1 {
            let group_size = exp2(group_size) as usize;
            for i in (0..self.values.len()).step_by(group_size) {
                let half_size = group_size / 2;
                for item_ind in 0..half_size {
                    self.values[i + half_size + item_ind] ^= self.values[i + item_ind];
                }
            }
        }
    }

    pub fn make_anf(&self) -> String {
        // тривиальные случаи
        if self.weight() == exp2(self.arg_count) {
            return String::from("1");
        } else if self.weight() == 0 {
            return String::from("0");
        }
        let mut self_mobius = self.clone();
        self_mobius.transform_mobius();
        let self_mobius = self_mobius;
        (0..exp2(self.arg_count))
            .map(|x| (x, self_mobius.evaluate(x).unwrap()))
            .filter(|(_, y)| y.is_set())
            .map(|(x, _)| x)
            .map(|x| {
                (0..self.arg_count)
                    .filter(|i| (x >> i) & 1 == 1)
                    .map(|i| format!("x{}", self.arg_count - i))
                    .collect::<Vec<String>>()
                    .join(" ")
            })
            .filter(|monoms| !monoms.is_empty())
            .map(|monom| format!("({})", monom))
            .collect::<Vec<String>>()
            .join(" + ")
    }

    pub fn degree(&self) -> u32 {
        let mut self_mobius = self.clone();
        self_mobius.transform_mobius();
        let self_mobius = self_mobius;
        (0..exp2(self.arg_count))
            .map(|x| (x, self_mobius.evaluate(x).unwrap()))
            .filter(|(_, y)| y.is_set())
            .map(|(x, _)| x)
            .map(|x| {
                let mut weight: u32 = 0;
                let mut x = x;
                while x > 0 {
                    weight += 1;
                    x = without_last_bit(x);
                }
                weight
            })
            .max()
            .unwrap_or(0)
    }

    pub fn characteristic_sequence(&self) -> Vec<i32> {
        (0..exp2(self.arg_count))
            .map(|x| match self.evaluate(x).unwrap() {
                Bit::One => -1,
                Bit::Zero => 1,
            })
            .collect()
    }

    pub fn walsh_hadamard_transform(&self) -> Vec<i32> {
        let mut result = self.characteristic_sequence();
        for group_size in 1..log2(result.len() as u32) + 1 {
            let group_size = exp2(group_size) as usize;
            for i in (0..result.len()).step_by(group_size) {
                let half_size = group_size / 2;
                for item_ind in 0..half_size {
                    let a = result[i + item_ind];
                    let b = result[half_size + i + item_ind];
                    result[i + item_ind] = a + b;
                    result[half_size + i + item_ind] = a - b;
                }
            }
        }
        result
    }

    pub fn cor(&self) -> u32 {
        let wht = self.walsh_hadamard_transform();
        for k in 1..=self.arg_count {
            let mut bc = BoolCombinations::init(self.arg_count, k);
            while let Some(a) = bc.next() {
                if wht[a as usize] != 0 {
                    return k - 1;
                }
            }
        }
        self.arg_count
    }

    /// сгенерировать линейную функцию из вектора коэффициентов
    pub fn linear(coef_vec: u32, arg_count: u32) -> Self {
        let mut f = Self::const0(arg_count);
        let mut coef_vec = coef_vec;
        while coef_vec != 0 {
            let hob = exp2(log2(coef_vec));
            set_bit(
                &mut f.values[div_word_bit_size(hob) as usize],
                mod_word_bit_size(hob),
            );
            coef_vec -= hob;
        }
        f.transform_mobius();
        f
    }

    /// инвертировать функцию
    pub fn invert(&mut self) {
        // не очень оптимальная инверсия функции
        self.values = Self::from_function(
            |x| match self.evaluate(x).unwrap() {
                Bit::Zero => Bit::One,
                Bit::One => Bit::Zero,
            },
            self.arg_count,
        )
        .values;
    }

    pub fn nonlinearity(&self) -> u32 {
        let wht = self.walsh_hadamard_transform();
        exp2(self.arg_count - 1) - (wht.iter().map(|x| x.abs() as u32).max().unwrap() / 2)
    }

    pub fn closest_affine_function(&self) -> Self {
        let wht = self.walsh_hadamard_transform();
        let (coef_vec, &wht_value) = wht
            .iter()
            .enumerate()
            .max_by_key(|(_, wht_value)| wht_value.abs())
            .unwrap();
        let mut affine_function = Self::linear(coef_vec as u32, self.arg_count);
        if wht_value < 0 {
            affine_function.invert();
        }
        affine_function
    }

    pub fn autocorrelation(&self) -> Vec<i32> {
        let mut result = self
            .walsh_hadamard_transform()
            .iter()
            .map(|x| x * x)
            .collect::<Vec<i32>>();
        for group_size in 1..log2(result.len() as u32) + 1 {
            let group_size = exp2(group_size) as usize;
            for i in (0..result.len()).step_by(group_size) {
                let half_size = group_size / 2;
                for item_ind in 0..half_size {
                    let a = result[i + item_ind];
                    let b = result[half_size + i + item_ind];
                    result[i + item_ind] = a + b;
                    result[half_size + i + item_ind] = a - b;
                }
            }
        }
        result.iter().map(|x| x >> self.arg_count).collect()
    }

    pub fn complete_nonlinearity(&self) -> u32 {
        exp2(self.arg_count - 2)
            - (self
                .autocorrelation()
                .iter()
                .skip(1)
                .map(|x| x.abs() as u32)
                .max()
                .unwrap()
                / 4)
    }

    pub fn pc(&self) -> u32 {
        let autocor = self.autocorrelation();
        for k in 1..=self.arg_count {
            let mut bc = BoolCombinations::init(self.arg_count, k);
            while let Some(a) = bc.next() {
                if autocor[a as usize] != 0 {
                    return k - 1;
                }
            }
        }
        self.arg_count
    }
}
