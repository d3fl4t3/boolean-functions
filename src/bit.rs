use std::ops::{Add, Mul};

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Bit {
    Zero = 0,
    One = 1,
}

impl Bit {
    pub fn from_u8(val: u8) -> Self {
        Self::from_u64(val as u64)
    }

    pub fn from_u32(val: u32) -> Self {
        Self::from_u64(val as u64)
    }

    pub fn from_u64(val: u64) -> Self {
        match val {
            0 => Self::Zero,
            1 => Self::One,
            _ => panic!("Conversion error"),
        }
    }

    pub fn is_set(self) -> bool {
        matches!(self, Self::One)
    }
}

impl Add for Bit {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self::from_u8(self as u8 ^ rhs as u8)
    }
}

impl Mul for Bit {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        Self::from_u8(self as u8 & rhs as u8)
    }
}
