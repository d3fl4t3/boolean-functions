mod bf;
mod bit;
#[cfg(test)]
mod tests;
pub mod utils;

pub use crate::bf::{BFError, BF};
pub use bit::Bit;
