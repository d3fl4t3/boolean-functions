use bf::BF;
use std::time::Instant;

fn main() {
    let mut f = BF::random(30);
    let t = Instant::now();
    f.autocorrelation();
    // для 30 аргументов получилось 103 секунды
    println!("autocorrelation time: {:?}", t.elapsed());
}
