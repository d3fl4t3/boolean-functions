pub const WORD_SIZE: u32 = std::mem::size_of::<u32>() as u32;
pub const WORD_BIT_SIZE: u32 = WORD_SIZE * 8;

#[inline]
pub const fn without_last_bit(x: u32) -> u32 {
    if x > 0 {
        x & (x - 1)
    } else {
        0
    }
}

#[inline]
pub const fn test_exp2(x: u32) -> bool {
    without_last_bit(x) == 0 && x > 0
}

#[inline]
pub const fn exp2(x: u32) -> u32 {
    1 << x
}

#[inline]
pub const fn mod_word_size(x: u32) -> u32 {
    x & (WORD_SIZE - 1)
}

#[inline]
pub const fn mod_word_bit_size(x: u32) -> u32 {
    x & (WORD_BIT_SIZE - 1)
}

#[inline]
pub const fn div_word_size(x: u32) -> u32 {
    x / WORD_SIZE
}

#[inline]
pub const fn div_word_bit_size(x: u32) -> u32 {
    x / WORD_BIT_SIZE
}

#[inline]
pub fn log2(x: u32) -> u32 {
    match x {
        0 | 1 => 0,
        mut x => {
            let mut c = 0;
            while x > 1 {
                x >>= 1;
                c += 1;
            }
            c
        }
    }
}

#[inline]
pub fn set_bit(x: &mut u32, y: u32) {
    *x = *x | exp2(y)
}

#[inline]
pub fn unset_bit(x: &mut u32, y: u32) {
    *x = *x & !exp2(y)
}

pub fn weight(v: u32) -> u32 {
    let mut weight = 0;
    let mut v = v;
    while v > 0 {
        weight += 1;
        v = without_last_bit(v);
    }
    weight
}

pub struct BoolCombinations {
    current_state: Option<u32>,
    n: u32,
    k: u32,
}

impl BoolCombinations {
    pub fn init(n: u32, k: u32) -> Self {
        BoolCombinations {
            current_state: None,
            n,
            k,
        }
    }

    pub fn next(&mut self) -> Option<u32> {
        let next_state = match self.current_state {
            Some(a) => {
                let b = (a + 1) & a;
                if b == 0 {
                    return None;
                }
                let c = weight((b - 1) ^ a) - 2;
                (((((a + 1) ^ a) << 1) + 1) << c) ^ b
            }
            None => ((1 << self.k) - 1) << (self.n - self.k),
        };
        self.current_state = Some(next_state);
        Some(next_state)
    }
}
