use super::bf::*;
use super::bit::*;
use super::utils::*;
use assert_matches::assert_matches;

pub fn test_rand_weight(lower_bound: u32, upper_bound: u32, test_count: u32) {
    for arg_count in lower_bound..upper_bound {
        let mut sum = 0f64;
        for _ in 0..test_count {
            let bf = BF::random(arg_count);
            let w = bf.weight();
            sum += w as f64;
        }
        let avg_weight = sum / test_count as f64;
        let probability = avg_weight / exp2(arg_count) as f64;
        println!(
            "arg_count = {}, sum = {}, avg_weight = {}, probability = {}",
            arg_count, sum, avg_weight, probability
        );
        assert!((probability - 0.5f64).abs() < 0.1f64);
    }
}

pub fn test_mobius_reversibility(lower_bound: u32, upper_bound: u32, test_count: u32) {
    for arg_count in lower_bound..upper_bound {
        for _ in 0..test_count {
            let f = BF::random(arg_count);
            let mut g = f.clone();
            g.transform_mobius();
            g.transform_mobius();
            assert!(f == g);
        }
    }
}

#[test]
fn exp2_works() {
    assert_eq!(exp2(0), 1);
    assert_eq!(exp2(1), 2);
    assert_eq!(exp2(2), 4);
}

#[test]
fn const0_works() {
    let bf = BF::const0(7);
    assert_eq!(bf.to_string(), "0".repeat(exp2(7) as usize));
    assert_eq!(bf.weight(), 0);
    assert_matches!(bf.evaluate(exp2(3) | exp2(4)), Ok(Bit::Zero));
    assert!(bf.evaluate(exp2(8)).is_err());
}

#[test]
fn const1_works() {
    let bf = BF::const1(7);
    assert_eq!(bf.to_string(), "1".repeat(exp2(7) as usize));
    assert_eq!(bf.weight(), exp2(7));
    assert_matches!(bf.evaluate(exp2(3) | exp2(4)), Ok(Bit::One));
    assert!(bf.evaluate(exp2(8)).is_err());
}

#[test]
fn user_func_works() {
    let bf = BF::from_string("0111").unwrap();
    assert_eq!(bf.to_string(), "0111");
    assert_eq!(bf.weight(), 3);
    assert_matches!(bf.evaluate(0), Ok(Bit::Zero));
    assert_matches!(bf.evaluate(1), Ok(Bit::One));
    assert_matches!(bf.evaluate(2), Ok(Bit::One));
    assert_matches!(bf.evaluate(3), Ok(Bit::One));
    assert!(bf.evaluate(4).is_err());
}

#[test]
fn only_valid_vecs_accepted() {
    assert!(BF::from_string("").is_err());
    assert!(BF::from_string("01a1").is_err());
    assert!(BF::from_string("011").is_err());
}

#[test]
fn rand_weight_is_avg_light() {
    test_rand_weight(1, 18, 100);
}

#[test]
#[ignore]
fn rand_weight_is_avg_hard() {
    test_rand_weight(18, 32, 1);
}

#[test]
fn mobius_is_reversible_light() {
    test_mobius_reversibility(1, 12, 10);
}

#[test]
#[ignore]
fn mobius_is_reversible_hard() {
    test_mobius_reversibility(30, 31, 1);
}

#[test]
fn mobius_transform_book_example_works() {
    let f = BF::from_string("01010011").unwrap();
    let mut g = f.clone();
    g.transform_mobius();
    assert_eq!(g.to_string(), "01000110");
    assert_eq!(f.make_anf(), "(x3) + (x3 x1) + (x2 x1)");
    assert_eq!(f.degree(), 2);
}

#[test]
fn mobius_transform_const0_anf() {
    let c0 = BF::const0(6);
    let mut c0_m = c0.clone();
    c0_m.transform_mobius();
    assert_eq!(c0_m.to_string(), "0".repeat(exp2(6) as usize));
    assert_eq!(c0.make_anf(), "0");
}

#[test]
fn mobius_transform_const1_anf() {
    let c1 = BF::const1(6);
    let mut c1_m = c1.clone();
    c1_m.transform_mobius();
    assert_eq!(
        c1_m.to_string(),
        "1".to_owned() + &"0".repeat(exp2(6) as usize - 1)
    );
    assert_eq!(c1.make_anf(), "1");
}

#[test]
fn walsh_hadamard_example1_works() {
    let bf = BF::from_string("11000000").unwrap();
    let tua = bf.walsh_hadamard_transform();
    assert_eq!(tua, [4, 0, -4, 0, -4, 0, -4, 0]);
}

#[test]
fn walsh_hadamard_example2_works() {
    let bf = BF::from_string("0111").unwrap();
    let tua = bf.walsh_hadamard_transform();
    assert_eq!(tua, [-2, 2, 2, 2]);
}

fn check_walsh_hadamard_for_const1(arg_count: u32) {
    let bf = BF::const1(arg_count);
    let tua = bf.walsh_hadamard_transform();
    let mut expected_tua = vec![0; exp2(arg_count) as usize];
    expected_tua[0] = -(exp2(arg_count) as i32);
    assert_eq!(tua, expected_tua);
}

#[test]
fn walsh_hadamard_for_const1_works() {
    check_walsh_hadamard_for_const1(3);
    check_walsh_hadamard_for_const1(6);
    check_walsh_hadamard_for_const1(9);
}

#[test]
fn bool_combinations_works() {
    let mut bc = BoolCombinations::init(10, 3);
    assert_eq!(bc.next().unwrap(), 0b1110000000);
    assert_eq!(bc.next().unwrap(), 0b1101000000);
    assert_eq!(bc.next().unwrap(), 0b1100100000);
    assert_eq!(bc.next().unwrap(), 0b1100010000);
    assert_eq!(bc.next().unwrap(), 0b1100001000);
    assert_eq!(bc.next().unwrap(), 0b1100000100);
    assert_eq!(bc.next().unwrap(), 0b1100000010);
    assert_eq!(bc.next().unwrap(), 0b1100000001);
    assert_eq!(bc.next().unwrap(), 0b1011000000);
    assert_eq!(bc.next().unwrap(), 0b1010100000);
}

#[test]
fn small_bool_combinations_works() {
    let mut bc = BoolCombinations::init(4, 2);
    assert_eq!(bc.next().unwrap(), 0b1100);
    assert_eq!(bc.next().unwrap(), 0b1010);
    assert_eq!(bc.next().unwrap(), 0b1001);
    assert_eq!(bc.next().unwrap(), 0b0110);
    assert_eq!(bc.next().unwrap(), 0b0101);
    assert_eq!(bc.next().unwrap(), 0b0011);
    assert!(bc.next().is_none());
}

#[test]
fn cor_example1_works() {
    let bf = BF::from_string("11000000").unwrap();
    assert_eq!(bf.cor(), 0);
}

#[test]
fn cor_example2_works() {
    let bf = BF::from_string("01101001").unwrap();
    assert_eq!(bf.cor(), 2);
}

#[test]
fn cor_example3_works() {
    let bf = BF::from_string("10111110").unwrap();
    assert_eq!(bf.cor(), 0);
}

#[test]
fn cor_example4_works() {
    let bf = BF::from_string("10011111").unwrap();
    assert_eq!(bf.cor(), 0);
}

#[test]
fn cor_const1_works() {
    let bf = BF::const1(10);
    assert_eq!(bf.cor(), 10);
}

#[test]
fn nonlinearity_example1_works() {
    let bf = BF::from_string("11000000").unwrap();
    assert_eq!(bf.nonlinearity(), 2);
}

#[test]
fn nonlinearity_example2_works() {
    let bf = BF::from_string("00000001").unwrap();
    assert_eq!(bf.nonlinearity(), 1);
}

#[test]
fn nonlinearity_example3_works() {
    let bf = BF::from_string("11111110").unwrap();
    assert_eq!(bf.nonlinearity(), 1);
}

#[test]
fn linear_example1_works() {
    let mut bf = BF::linear(0b111, 3);
    bf.transform_mobius();
    assert_eq!(bf.to_string(), "01101000");
}

#[test]
fn linear_example2_works() {
    let mut bf = BF::linear(0, 3);
    bf.transform_mobius();
    assert_eq!(bf.to_string(), "00000000");
}

#[test]
fn linear_example3_works() {
    let mut bf = BF::linear(0b101, 3);
    bf.transform_mobius();
    assert_eq!(bf.to_string(), "01001000");
}

#[test]
fn closest_affine_example1_function_works() {
    let bf = BF::from_string("01111010").unwrap();
    let af = bf.closest_affine_function();
    assert_eq!(af.make_anf(), "(x3) + (x1)");
}

#[test]
fn closest_affine_example2_function_works() {
    let bf = BF::from_string("00000001").unwrap();
    let af = bf.closest_affine_function();
    assert_eq!(af.make_anf(), "0");
}

#[test]
fn closest_affine_example3_function_works() {
    let bf = BF::from_string("11111110").unwrap();
    let af = bf.closest_affine_function();
    assert_eq!(af.make_anf(), "1");
}

#[test]
fn autocorrelation_example1_works() {
    let bf = BF::from_string("11000000").unwrap();
    assert_eq!(bf.autocorrelation(), [8, 8, 0, 0, 0, 0, 0, 0]);
}

#[test]
fn complete_nonlinearity_example1_works() {
    let bf = BF::from_string("11000000").unwrap();
    assert_eq!(bf.complete_nonlinearity(), 0);
}

#[test]
fn complete_nonlinearity_example2_works() {
    let bf = BF::from_string("00111010").unwrap();
    assert_eq!(bf.complete_nonlinearity(), 0);
}

#[test]
fn complete_nonlinearity_example3_works() {
    let bf = BF::from_string("0001000100011110").unwrap();
    assert_eq!(bf.complete_nonlinearity(), 4);
}

#[test]
fn complete_nonlinearity_example4_works() {
    let bf = BF::from_string("0001000100011110000100010001111000010001000111101110111011100001")
        .unwrap();
    assert_eq!(bf.complete_nonlinearity(), 16);
}

#[test]
fn pc_example1_works() {
    let bf = BF::from_string("0001000100011110").unwrap();
    assert_eq!(bf.pc(), 4);
}

#[test]
fn pc_example2_works() {
    let bf = BF::from_string("0001000100011110000100010001111000010001000111101110111011100001")
        .unwrap();
    assert_eq!(bf.pc(), 6);
}

#[test]
fn pc_example3_works() {
    let bf = BF::from_string("11000000").unwrap();
    assert_eq!(bf.pc(), 0);
}

#[test]
fn pc_example4_works() {
    let bf = BF::from_string("01110111").unwrap();
    assert_eq!(bf.pc(), 0);
}
